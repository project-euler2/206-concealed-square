import time
start_time = time.time()

i = 138902663 #maximum number to check
while not all(int(str(i**2)[x*2]) == x+1 for x in range(9)): #We check if the number has the correct shape
    i -= 2 #We only test the odd values (we know it must end with 3 or 7)
print(i*10) #We print the result

print(f"--- {(time.time() - start_time):.10f} seconds ---" )